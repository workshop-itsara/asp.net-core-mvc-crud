using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using dotnet_mvc_crud.Data;
using dotnet_mvc_crud.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace dotnet_mvc_crud.Controllers
{
    // [Route("[controller]")]
    public class StudentController : Controller
    {
        private readonly ApplicationDBContext _db;

        public StudentController(ApplicationDBContext db)
        {
            _db = db;
        }

        public IActionResult Index()
        {
            // StudentModel s1 = new StudentModel();
            // s1.Id = 1;
            // s1.Name = "Tony Stark";
            // s1.Score = 100;

            // var s2 = new StudentModel();
            // s2.Id = 2;
            // s2.Name = "Steve Rogers";
            // s2.Score = 45;

            // StudentModel s3 = new();
            // s3.Id = 1;
            // s3.Name = "Thor";
            // s3.Score = 70;

            // List<StudentModel> student = new List<StudentModel>();
            // student.Add(s1);
            // student.Add(s2);
            // student.Add(s3);

            // return View(student);
            IEnumerable<StudentModel> student = _db.Students;
            return View(student);
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(StudentModel data)
        {
            if (ModelState.IsValid)
            {
                _db.Students.Add(data);
                _db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(data);
        }

        public IActionResult Update(int? id)
        {
            if (id == null || id == 0)
            {
                return NotFound();
            }

            var data = _db.Students.Find(id);

            if (data == null)
            {
                return NotFound();
            }

            return View(data);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Update(StudentModel data)
        {
            if (ModelState.IsValid)
            {
                _db.Students.Update(data);
                _db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(data);
        }

        public IActionResult Delete(int? id)
        {
            if (id == null || id == 0)
            {
                return NotFound();
            }

            var data = _db.Students.Find(id);

            if (data == null)
            {
                return NotFound();
            }

            _db.Students.Remove(data);
            _db.SaveChanges();
            return RedirectToAction("Index");
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View("Error!");
        }
    }
}