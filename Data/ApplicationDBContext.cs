using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using dotnet_mvc_crud.Models;
using Microsoft.EntityFrameworkCore;

namespace dotnet_mvc_crud.Data
{
    public class ApplicationDBContext : DbContext
    {
        public ApplicationDBContext(DbContextOptions<ApplicationDBContext> options) : base(options)
        {

        }

        public DbSet<StudentModel> Students { get; set; }
    }
}