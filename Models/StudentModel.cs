using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace dotnet_mvc_crud.Models
{
    public class StudentModel
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "กรุณากรอกชื่อนักเรียน")]
        [DisplayName("ชื่อนักเรียน")]
        public string Name { get; set; }

        [Range(0, 100, ErrorMessage = "กรุณาป้อนคะแนนสอบระหว่าง 0-100")]
        [DisplayName("คะแนนสอบ")]
        public int Score { get; set; }
    }
}