<div align="center">
    <img src="https://upload.wikimedia.org/wikipedia/commons/e/ee/.NET_Core_Logo.svg" width="100">
</div>

# ASP.NET Core MVC - CRUD

Web App for Create, Read, Update, Delete with ASP.NET Core MVC and Microsoft SQL Server

## Table of Contents

- [Prerequisites](#prerequisites)
- [Configuration](#configuration)
- [Installation](#installation)
- [Usage](#usage)
- [Credits](#credits)
- [License](#license)

## Prerequisites

* .NET Core : 7.0
* Microsoft SQL Server : Microsoft Azure SQL Edge Developer (RTM) - 15.0.2000.1574 (ARM64)
* Microsoft.EntityFrameworkCore : 7.0.9
* Microsoft.EntityFrameworkCore.SqlServer : 7.0.9
* Microsoft.EntityFrameworkCore.Tools : 7.0.9

## Configuration

* Port : 5056

## Installation



## Usage



## Credits

Itsara Rakchanthuek

## License

[MIT](LICENSE)